	export default class Hamburger{
		constructor(menu, showBtn, closeBtn) {
			this.menu = document.querySelector(menu)
			this.showBtn = document.querySelector(showBtn)
			this.closeBtn = document.querySelector(closeBtn)
		}
		close(){
			this.closeBtn.addEventListener('click',() => {
				this.menu.classList.remove('active')
			})
		}
		show(){
			this.showBtn.addEventListener('click',() => {
				this.menu.classList.add('active')
			})
		}
	}
	