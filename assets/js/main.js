import Hamburger from "/assets/js/hamburger.js";
import Popup from "/assets/js/popup.js";
import Calculator from "/assets/js/calculator.js";
window.addEventListener('DOMContentLoaded', () => {
    const showMenu = new Hamburger('.nav_wrap','.open_wrap_btn','.btn_close')
    showMenu.show()
    showMenu.close()
    const pop = new Popup('.text_number', '.overlay-pop', '.img-close-pop')
    pop.closePopap()
    pop.show()
    const calc = new Calculator('.selection_area_item', '.flex_selection_area')
    calc.show()
const swiperMain = new Swiper('.main_swiper', {
  loop: true,
  slidesPerView: 1,
  // autoplay: {
  //   delay: 8000,
  // },
  breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1
      },
      // when window width is >= 480px
      920: {
        slidesPerView: 1
      },
      // when window width is >= 640px
      1000: {
        slidesPerView: 1
      },
      1260:{
        slidesPerView: 1
      }
    },
  pagination: {
      el: '#main_pag',
      type: 'bullets',
      clickable: true
    }
});
  const swiperDoctors = new Swiper('.swiper_doctors', {
    loop: false,
    spaceBetween: 20,
    slidesPerView: 4,
    breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 2
        },
        // when window width is >= 480px
        920: {
          slidesPerView: 2
        },
        // when window width is >= 640px
        1000: {
          slidesPerView: 4
        },
        1260:{
          slidesPerView: 4
        }
      },
      navigation: {
        nextEl: '.swiper-buttondoc-next',
        prevEl: '.swiper-buttondoc-prev',
      }
  });
  const swiperEquipment = new Swiper('.swiper_equipment', {
    loop: false,
    spaceBetween: 430,
    slidesPerView: 2,
    breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 2
        },
        // when window width is >= 480px
        920: {
          slidesPerView: 2
        },
        // when window width is >= 640px
        1000: {
          slidesPerView: 2
        },
        1260:{
          slidesPerView: 2
        }
      },
      navigation: {
        nextEl: '.swiper-btnequipment-next',
        prevEl: '.swiper-btnequipment-prev',
      }
  });
})